#!/bin/sh
json_to_markdown_dir=./tools
markdowns=./markdowns
tags=./tags
for source in $tags/*
do
    # get the file "source" prefix
    source_basename=$(echo "$source" | sed -r "s/.+\/(.+)\..+/\1/")
    ruby $json_to_markdown_dir/json_to_markdown.rb $source $markdowns/$source_basename.markdown
done
