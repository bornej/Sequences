---
title: "Running"
duration: "10:28"
artist: "Borne Jonathan"
album: "Sequences"
genre: "Electronic music"
copyright: "Running Copyright (C) 2016  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Sequences/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Running"
---
