---
title: "Tronica"
duration: "5:29"
artist: "Borne Jonathan"
album: "Sequences"
genre: "Electronic music"
copyright: "Tronica Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Sequences/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Tronica"
---
