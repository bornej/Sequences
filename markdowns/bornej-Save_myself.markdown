---
title: "Save myself"
duration: "4:48"
artist: "Borne Jonathan"
album: "Sequences"
genre: "Electronic music"
copyright: "Save myself Copyright (C) 2018  Borne Jonathan"
repository_url: "https://gitlab.com/bornej/Sequences/"
contact: borne.jonathan@protonmail.com
file_basename: "bornej-Save_myself"
---
