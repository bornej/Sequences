#!/bin/sh
jsontag_v2_dir=./tools
sources_flacs=./sources_flacs
tags=./tags
for source in $tags/*
do
    echo $source
    # get the file "source" prefix
    source_basename=$(echo "$source" | sed -r "s/.+\/(.+)\..+/\1/")
    echo $source_basename
    for file in $sources_flacs/$source_basename"_sources"/*
    do
        #echo $file
        metaflac --remove-all $file
        ruby $jsontag_v2_dir/jsontag_v2.rb $source $file
    done
done
